// 5
const http = require("http");
// 6
const port = 3000;
// 7
const server = http.createServer( (req, res) => {
	// 9
	if(req.url == '/login'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("You are in the Login page");
	}
	// 10
	else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Error 404: Page not found');
	}
});
server.listen(port);
// 8
console.log(`Server now accessible at localhost:${port}.`);